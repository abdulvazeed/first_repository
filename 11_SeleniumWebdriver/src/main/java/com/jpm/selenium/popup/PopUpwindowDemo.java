package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpwindowDemo {

		public static void main(String[] args) throws InterruptedException {
			//Load driver and open browser
			WebDriver driver=ChromeUtil.getChromeDriver();
			String url="D:\\Abdul\\Day 51\\11_SeleniumWebdriver\\src\\main\\java\\PopUpWinDemo.html";
			driver.get(url);
			String parentWin=driver.getWindowHandle();
			System.out.println("Parent Win : "+parentWin);
			Thread.sleep(2000);
			driver.findElement(By.id("newtab")).click();
			parentWin=driver.getWindowHandle();
			System.out.println("Parent Win : "+parentWin);
			Thread.sleep(2000);
			Set<String>childWinList=driver.getWindowHandles();//all the window user
			for(String childWin : childWinList){
				if(!childWin.equals(parentWin)){
					Thread.sleep(2000);
					driver.switchTo().window(childWin);
					System.out.println("Child Window : "+childWin);
					driver.findElement(By.id("alert")).click();
					Thread.sleep(2000);
					Alert alert = driver.switchTo().alert();
					Thread.sleep(2000);
					alert.accept(); // use dismiss() method to cancel
					
				}
			}
			driver.switchTo().window(parentWin);
			System.out.println("Swith to parent Win : "+parentWin);
			Thread.sleep(2000);
			System.out.println("Again click to another child Win");
			//driver.findElement(By.id("new window");
			for (String childWin2: driver.getWindowHandles()) {
				if(!childWin2.equals(parentWin)) {
					driver.switchTo().window(childWin2);
					System.out.println("Child Window 2: " +childWin2);
					Thread.sleep(2000);
				}
				
			}
			driver.quit();
			
			
	}

}
