package com.jpm.selenium.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jpm.selenium.util.ChromeUtil;

public class WorkingWithForms {

	public WorkingWithForms() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver=ChromeUtil.getChromeDriver();
		driver.get("file:\\D:\\Abdul\\Day 51\\11_SeleniumWebdriver\\src\\main\\java\\WorkingWithForms.html");
			//Maximize
			driver.manage().window().maximize();
			driver.findElement(By.id("txtUserName")).sendKeys("JPM_Abdul");
			driver.findElement(By.id("txtPassword")).sendKeys("jpm@123");//pwd
			driver.findElement(By.className("Format")).sendKeys("jpm@123"); //confirm pwd
			driver.findElement(By.cssSelector("Input.Format1")).sendKeys("Abdul");
			driver.findElement(By.cssSelector("Input#txtLastName")).sendKeys("Vazeed");
			List<WebElement>radioElements=driver.findElements(By.name("gender"));
			for(WebElement radio:radioElements) {
				String radioSelection=radio.getAttribute("value").toString();
				//which radio button to be selected
				if(radioSelection.equals("Male"));
				Thread.sleep(2000);
				radio.click();
	driver.findElement(By.cssSelector("input[type=date]")).sendKeys("12/12/2001");
	driver.findElement(By.id("input.Format[name='Email']")).sendKeys("abd@gmail.com");
	//dropdown
	driver.findElement(By.name("Address")).sendKeys("Hyderabad");
	Select drpCity=new Select(driver.findElement(By.name("City")));
	//dropdwon 3 ways -> selectyByIndex, selectByValue, selectByVisibleText
	drpCity.selectByIndex(4);
	drpCity.selectByValue("Hyderabad");
	drpCity.selectByValue("Mumbai");
	driver.findElement(By.name("Phone")).sendKeys("7416778866");
	List<WebElement> checkEleList=driver.findElements(By.name("chkHobbies"));
	for(WebElement hobbyChkBox:checkEleList){
		String selection=hobbyChkBox.getAttribute("value").toString();
		if (!selection.equals("Movies")){ //not click Movies
			hobbyChkBox.click();
			
		}
		
	}
		
	Thread.sleep(10000);
				driver.close();
	System.out.println("Registration on process!");
	}			
}
}